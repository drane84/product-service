package com.example.demo.DTO;

import java.util.List;

public class ProductReviewDTO {

	private ProductDTO productDTO;
	
	private List<ReviewDTO> reviewList;

	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}

	public List<ReviewDTO> getReviewList() {
		return reviewList;
	}

	public void setReviewList(List<ReviewDTO> reviewList) {
		this.reviewList = reviewList;
	}
	
	
}
