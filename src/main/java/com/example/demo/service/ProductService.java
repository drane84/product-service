package com.example.demo.service;

import com.example.demo.DTO.ProductDTO;
import com.example.demo.DTO.ProductReviewDTO;

public interface ProductService {

	/**
	 * Creates the product.
	 *
	 * @param productDTO the product DTO
	 * @return the product DTO
	 */
	public ProductDTO createProduct(ProductDTO productDTO);
	
	/**
	 * Gets the product.
	 *
	 * @param productId the product id
	 * @param fetchReviews the fetch reviews
	 * @return the product
	 */
	public ProductReviewDTO getProduct(Long productId,Boolean fetchReviews);
}
