package com.example.demo.service.impl;

import java.util.Arrays;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.DTO.ProductDTO;
import com.example.demo.DTO.ProductReviewDTO;
import com.example.demo.DTO.ReviewDTO;
import com.example.demo.model.ProductModel;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;


@Service("productService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private RestTemplate restTemplate;
	
	 @Override
	public ProductDTO createProduct(ProductDTO productDTO) {
		ProductModel productModel = new ProductModel();
		BeanUtils.copyProperties(productDTO, productModel);
		productRepository.save(productModel);
		productDTO.setProductID(productModel.getProductID());
		return productDTO;
	}

	@Override
	public ProductReviewDTO getProduct(Long productId,Boolean featchReviews) {
		ProductDTO dto = new ProductDTO();
		ProductReviewDTO prdreveiwDTO  = new  ProductReviewDTO();
		ProductModel productModel =  this.productRepository.getOne(productId);
		BeanUtils.copyProperties(productModel, dto);
		prdreveiwDTO.setProductDTO(dto);
		if(featchReviews) {
			  HttpHeaders headers = new HttpHeaders();
		      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		      HttpEntity <String> entity = new HttpEntity<String>(headers);
		      
			ResponseEntity<ReviewDTO[]> response= restTemplate.getForEntity("http://localhost:9002/api/v1/review/getReviewByProductId/"+productId,ReviewDTO[].class);
			ReviewDTO[] reviews = response.getBody();
			prdreveiwDTO.setReviewList(Arrays.asList(reviews));
			return prdreveiwDTO;
		}else {
			return prdreveiwDTO;
		}
	}

}