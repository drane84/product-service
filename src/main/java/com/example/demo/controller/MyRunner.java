package com.example.demo.controller;

public class MyRunner implements Runnable{

	@Override
	public void run() {
		Double result = 0.0d;
		 Long cnt = 1L;
		 while(true) {
			 result += Math.sqrt(new Double(cnt));
			 System.out.println("Current Result is "+ result);
			 cnt++;
		 }
		
	}

}
