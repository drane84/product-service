package com.example.demo.controller;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.ProductDTO;
import com.example.demo.DTO.ProductReviewDTO;
import com.example.demo.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1/product")
@Api(value = "Product APIs", tags = "Products")
public class ProductController {
 
	private boolean stopExecution=false;
	@Autowired
	private ProductService productService;

	/**
	 * Creates the User.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	@ApiOperation(nickname = "createProduct", value = "Create Product", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Product created successfully") })
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO productDTO) {
		productDTO = this.productService.createProduct(productDTO);
		return new ResponseEntity<ProductDTO>(productDTO, HttpStatus.OK);
	}
	
	@ApiOperation(nickname = "getProduct/{Id}/{fetchReviews}", value = "Fetch Product", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Product fetched successfully") })
	@RequestMapping(value = "/{Id}/{fetchReviews}",method = RequestMethod.GET)
	public ResponseEntity<ProductReviewDTO> getUser( @ApiParam(name = "Id", value = "Unique Id of Product", required = true)@PathVariable(name = "Id")Long productId,
			@ApiParam(name = "fetchReviews", value = "Fetch review (true/false)", required = true)@PathVariable(name = "fetchReviews")Boolean fetchReviews
			) {
		ProductReviewDTO productReviewDTO = this.productService.getProduct(productId,fetchReviews);
		return new ResponseEntity<ProductReviewDTO>(productReviewDTO, HttpStatus.OK);	
	}
	
	@ApiOperation(nickname = "load", value = "Load Product Service", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Product Loaded successfully") })
	@RequestMapping(value = "/{loadCount}",method = RequestMethod.POST)
	public ResponseEntity<Void> loadProductService( @ApiParam(name = "loadCount", value = "Load Parameter", required = true)@PathVariable(name = "loadCount")Long loadCount) {
		Double result = 0.0d;
		
		for (int i = 1;i< loadCount;i++) {
			
			result += Math.sqrt(new Double(i));
		}
	  
		return new ResponseEntity( HttpStatus.OK);
	}
	
	@ApiOperation(nickname = "loadData", value = "Load Product Service Data", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Product Loaded successfully") })
	@RequestMapping(value = "/loadData/{stop}",method = RequestMethod.POST)
	public ResponseEntity<Void> loadProductService( @ApiParam(name = "stop", value = "Stop Loading Parameter", required = true)@PathVariable(name = "stop")Boolean stop) {
		
		System.out.println("Recieved Request to generate load" + stop); 
		Thread t1 = new Thread(new MyRunner());
		 t1.start();
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
